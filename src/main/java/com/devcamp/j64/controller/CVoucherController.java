package com.devcamp.j64.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j64.model.CVoucher;
import com.devcamp.j64.repository.IVoucherRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CVoucherController {
	@Autowired
	IVoucherRepository pVoucherRepository;
    
    @GetMapping("/vouchers")
	public ResponseEntity<List<CVoucher>> getAllVouchers() {
		try {
			List<CVoucher> pVouchers = new ArrayList<CVoucher>();
            pVoucherRepository.findAll().forEach(pVouchers::add);
			//TODO: Hãy code lấy danh sách voucher từ DB
			return new ResponseEntity<>(pVouchers, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @GetMapping("/vouchers/{id}")
	public ResponseEntity<CVoucher> getCVoucherById(@PathVariable("id") long id) {
		//Todo: viết code lấy voucher theo id tại đây
        Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
		if (voucherData.isPresent()) {
			return new ResponseEntity<>(voucherData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

    @PostMapping("/vouchers")
	public ResponseEntity<Object> createCVoucher(@RequestBody CVoucher pVouchers) {
		try {
            Optional<CVoucher> voucherData = pVoucherRepository.findById(pVouchers.getId());
            if(voucherData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Voucher already exsit  ");
            }
            // pVouchers.setNgayTao(new Date());
			// pVouchers.setNgayCapNhat(null);
			CVoucher _vouchers = pVoucherRepository.save(pVouchers);
			//TODO: Hãy viết code tạo voucher đưa lên DB
			return new ResponseEntity<>(_vouchers, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @PutMapping("/vouchers/{id}")
	public ResponseEntity<Object> updateCVoucherById(@PathVariable("id") long id, @RequestBody CVoucher pVouchers) {
        Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
		//TODO: Hãy viết code lấy voucher từ DB để UPDATE
		if (voucherData.isPresent()) {
            CVoucher voucher= voucherData.get();
			voucher.setMaVoucher(pVouchers.getMaVoucher());
			voucher.setPhanTramGiamGia(pVouchers.getPhanTramGiamGia());
			voucher.setGhiChu(pVouchers.getGhiChu());
			try {
                return new ResponseEntity<>(pVoucherRepository.save(voucher), HttpStatus.OK);	
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity().body("Failed to Update specified Voucher:"+e.getCause().getCause().getMessage());
            }
        } else {
            return ResponseEntity.badRequest().body("Failed to get specified Voucher: "+id + "  for update.");
        }
	}

    @DeleteMapping("/vouchers/{id}")
	public ResponseEntity<CVoucher> deleteCVoucherById(@PathVariable("id") long id) {
		try {
            pVoucherRepository.deleteById(id);
			//TODO: Hãy viết code xóa 1 voucher từ DB
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @DeleteMapping("/vouchers")
	public ResponseEntity<CVoucher> deleteAllCVoucher() {
		try {
            pVoucherRepository.deleteAll();
			//TODO: Hãy viết code xóa all voucher từ DB
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}