package com.devcamp.j64.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j64.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {

}